module NavigationHelpers
  def path_to(page_name)
    case page_name

    when /^the home\s?page$/
      '/'
    when /the new user page/
      '/users/new'

    when /the home page/
      '/'

    when /the editing page/
      '/users/1/edit'

    when /the post page/
      '/posts/new'

    when /the edit post page/
      '/posts/1/edit'


    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
                  "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
end

World(NavigationHelpers)